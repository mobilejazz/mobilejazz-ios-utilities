//
//  MJBlockingCache.m
//
//  Created by gimix on 9/13/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import "MJBlockingCache.h"

@interface MJBlockingCache()

@property (strong, nonatomic) NSMutableDictionary* d;
/** Dictionary that contains one lock for every key that is being written */
@property (strong, nonatomic) NSMutableDictionary* keyLocks;

@end

@implementation MJBlockingCache

-(id)init {
    self = [super init];
    if(self) {
        self.d = [[NSMutableDictionary alloc] init];
        self.keyLocks = [[NSMutableDictionary alloc] init];
        
        // register for low memory conditions
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMemoryWarning:) name: UIApplicationDidReceiveMemoryWarningNotification object:nil];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(id)getKeyFromCacheOnly:(NSString*)key
{
    // get the value
    id value;
    @synchronized(self.d) {
        value = [self.d objectForKey:key];
    }
    return value; // done!
}

-(id)getKeyFromCache:(NSString*)key OrFromBlock:(id (^)())block
{
    // lock if other writers are working
    NSLock* lock;
    @synchronized(self.keyLocks) {
        lock = [self.keyLocks valueForKey:key];
    }
    [lock lock];
    [lock unlock];// unlock immediately, we just wanted to wait

    // get the value
    id value;
    @synchronized(self.d) {
        value = [self.d objectForKey:key];        
    }
    if(value)
        return value; // done!

    // if no value, then get it from block()
    // lock other readers
    @synchronized(self.keyLocks) {
        lock = [NSLock new];
        [self.keyLocks setObject:lock forKey:key];
        [lock lock]; // will never block here
    }
    value = block();
    if(value) {
        [self.d setObject:value forKey:key];
    }
    @synchronized(self.keyLocks) {
        [self.keyLocks removeObjectForKey:key];
    }
    [lock unlock]; // will wake up readers
    return value;
}

-(void)getKeyFromCache:(NSString*)key OrFromBlock:(id (^)())calculationBlock ThenExecute:(void (^)(id value))thenBlock
{
    id value = [self getKeyFromCacheOnly:key];
    if(value) { // run thenBlock immediately
        thenBlock(value);
    } else { // dispatch a background thread to fetch the result, then perform thenBlock in this thread
        dispatch_queue_t currentQueue = dispatch_get_current_queue();
//        dispatch_retain(currentQueue); //ARC not available for queues
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            id value = [self getKeyFromCache:key OrFromBlock:calculationBlock];
            dispatch_async(dispatch_get_main_queue(), ^{
                thenBlock(value);
            });
//            dispatch_release(currentQueue);
        });
    }
}

-(void)clean
{
    @synchronized(self.d) {
        [self.d removeAllObjects];        
    }
}

#pragma mark - Memory warning
- (void) handleMemoryWarning:(NSNotification *)notification
{
}
@end
