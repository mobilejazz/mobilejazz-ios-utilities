//
//  UISearchBar+Keyboard.m
//
//  Created by gimix on 10/11/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import "UISearchBar+Keyboard.h"

// http://stackoverflow.com/questions/2705865/change-uisearchbar-keyboard-search-button-title
@implementation UISearchBar (Keyboard)

-(void)setKeyboardReturnKeyType:(UIReturnKeyType)returnKeyType
{
    [self.searchBarTextField setReturnKeyType:returnKeyType];
}

-(void)setKeyboardAppearance:(UIKeyboardAppearance)keyboardAppearance
{
    [self.searchBarTextField setKeyboardAppearance:keyboardAppearance];
}

-(UITextField*)searchBarTextField
{
    // Set the return key and keyboard appearance of the search bar
    for (UIView *searchBarSubview in [self subviews]) {
        if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
            @try {
                return (UITextField *)searchBarSubview;
            }
            @catch (NSException * e) {
                // ignore exception
            }
        }
    }
    return nil;
}
@end
