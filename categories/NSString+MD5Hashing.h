//
//  NSString+MD5Hashing.h
//
//  Created by Stefan Klumpp on 11/6/12.
//  Copyright (c) 2012 Mobile Jazz, S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5Hashing)

- (NSString *)md5;

@end
