//
//  UIControl+DDBlockActions.h
//
//  Created by gimix on 10/16/12.
//  Copyright (c) 2012 dtime. All rights reserved.

// http://stackoverflow.com/questions/4581782/can-i-pass-a-block-as-a-selector-with-objective-c

#import <UIKit/UIKit.h>

@interface UIControl (DDBlockActions)

- (void) addEventHandler:(void(^)(void))handler forControlEvents:(UIControlEvents)controlEvents;

@end