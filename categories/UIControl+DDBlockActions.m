//
//  UIControl+DDBlockActions.m
//
//  Created by gimix on 10/16/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import "UIControl+DDBlockActions.h"
#import <objc/runtime.h>

@interface DDBlockActionWrapper : NSObject
@property (nonatomic, copy) void (^blockAction)(void);
- (void) invokeBlock:(id)sender;
@end

@implementation DDBlockActionWrapper
@synthesize blockAction;
- (void) dealloc {
    [self setBlockAction:nil];
}

- (void) invokeBlock:(id)sender {
    [self blockAction]();
}
@end

@implementation UIControl (DDBlockActions)

static const char * UIControlDDBlockActions = "UIControlDDBlockActions";

- (void) addEventHandler:(void(^)(void))handler forControlEvents:(UIControlEvents)controlEvents {
    
    NSMutableArray * blockActions = objc_getAssociatedObject(self, &UIControlDDBlockActions);
    if (blockActions == nil) {
        blockActions = [NSMutableArray array];
        objc_setAssociatedObject(self, &UIControlDDBlockActions, blockActions, OBJC_ASSOCIATION_RETAIN);
    }
    
    DDBlockActionWrapper * target = [[DDBlockActionWrapper alloc] init];
    [target setBlockAction:handler];
    [blockActions addObject:target];
    
    [self addTarget:target action:@selector(invokeBlock:) forControlEvents:controlEvents];
}

@end