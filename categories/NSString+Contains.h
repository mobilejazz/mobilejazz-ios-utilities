//
//  NSString+Contains.h
//
//  Created by Stefan Klumpp on 2/20/12.
//  Copyright (c) 2012 Mobile Jazz, S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (contains)

- (BOOL) contains:(NSString *)searchString;

@end
