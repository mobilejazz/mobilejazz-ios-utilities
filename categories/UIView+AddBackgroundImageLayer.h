//
//  UIView+AddBackgroundImageLayer.h
//
//  Created by Stefan Klumpp on 9/7/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (AddBackgroundImageLayer)

-(void)addBackgroundImageLayer:(UIImage *)image clearColor:(BOOL)clearColor;

@end
