//
//  NSArray+Mapping.m
//
//  Created by Jordi Giménez Gámez on 3/23/12.
//  Copyright (c) 2012 Mobile Jazz, S.L. All rights reserved.
//

#import "NSArray+Mapping.h"

@implementation NSArray (Mapping)

-(NSArray *)arrayByMappingObjectsWithBlock:(id (^)(id obj))block;
{
    NSMutableArray *newArray = [NSMutableArray arrayWithCapacity:self.count];
    for ( id obj in self ) [newArray addObject:block(obj)];
    return newArray;
}

@end
