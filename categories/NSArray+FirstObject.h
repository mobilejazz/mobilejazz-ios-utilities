//
//  NSArray_FirstObject.h
//  http://troybrant.net/blog/2010/02/adding-firstobject-to-nsarray/

#import <Foundation/Foundation.h>

@interface NSArray (FirstObject)

- (id)firstObject;

@end 