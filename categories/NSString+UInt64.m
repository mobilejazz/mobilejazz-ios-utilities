//
//  NSString+UInt64.m
//
//  Created by Stefan Klumpp on 2/28/12.
//  Copyright (c) 2012 Mobile Jazz, S.L. All rights reserved.
//

#import "NSString+UInt64.h"

@implementation NSString (uint64)


- (UInt64) uInt64Value
{
	return strtoull([self UTF8String], NULL, 0);	
}

+ (NSString *) stringFromUInt64:(UInt64)number
{
	NSNumber *num = [NSNumber numberWithUnsignedLongLong:number];
	return [NSString stringWithFormat:@"%llu", [num unsignedLongLongValue]];	
}

@end
