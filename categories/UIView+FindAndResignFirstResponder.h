//
//  UIView+FindAndResignFirstResponder.h
// http://stackoverflow.com/questions/1823317/how-do-i-legally-get-the-current-first-responder-on-the-screen-on-an-iphone

#import <UIKit/UIKit.h>

@interface UIView (FindAndResignFirstResponder)

- (BOOL)findAndResignFirstResponder;

@end
