//
//  UISearchBar+Keyboard.h
//
//  Created by gimix on 10/11/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISearchBar (Keyboard)

-(void)setKeyboardReturnKeyType:(UIReturnKeyType)returnKeyType;
-(void)setKeyboardAppearance:(UIKeyboardAppearance)keyboardAppearance;
-(UITextField*)searchBarTextField;

@end
