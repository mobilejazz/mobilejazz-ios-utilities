//
//  UIView+AddBackgroundImageLayer.m
//
//  Created by Stefan Klumpp on 9/7/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import "UIView+AddBackgroundImageLayer.h"

@implementation UIView (AddBackgroundImageLayer)

-(void)addBackgroundImageLayer:(UIImage *)image clearColor:(BOOL)clearColor
{
	UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
	backgroundImageView.image = image;
    [self addSubview:backgroundImageView];
    [self sendSubviewToBack:backgroundImageView];

	if (clearColor)
		self.backgroundColor = [UIColor clearColor];
}

@end
