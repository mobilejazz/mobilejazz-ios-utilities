//
//  NSAttributedString+MJHTML.h
//
//  Created by gimix on 8/29/12.
//  Copyright (c) 2012 Mobile Jazz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (MJHTML)

/** Creates an NSAttributedString out of an NSString containing basic
 HTML tags. Only <b> and <br/> tags are supported at the moment.
 */
+(NSAttributedString*) attributedStringWithHtml:(NSString*)html font:(UIFont*)font boldFont:(UIFont*)boldFont;

@end

@interface NSMutableAttributedString (MJHTML)

/** Creates an NSMutableAttributedString out of an NSString containing basic
 HTML tags. Only <b> and <br/> tags are supported at the moment.
 */
+(NSMutableAttributedString*) attributedStringWithHtml:(NSString*)html font:(UIFont*)font boldFont:(UIFont*)boldFont;

@end
