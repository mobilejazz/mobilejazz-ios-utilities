//
//  NSString+Contains.m
//
//  Created by Stefan Klumpp on 2/20/12.
//  Copyright (c) 2012 Mobile Jazz, S.L. All rights reserved.
//

#import "NSString+Contains.h"

@implementation NSString (contains)

- (BOOL) contains:(NSString *)searchString {	
	return ([self rangeOfString:searchString].location != NSNotFound);
}

@end
