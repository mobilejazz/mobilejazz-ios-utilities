//
//  NSDictionary+SetObject.h
//
//  Created by gimix on 8/23/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SetObject)

/** @abstract Constructor to a new immutable Dictionary by adding an object to an existing
 * Dictionary.
 *
 * Makes sense for small changes to an immutable Dictionary.</br>
 * Functionally similar to NSMutableDictionary#setObject:forKey:
 *
 * @param dictionary Base Dictionary that needs an object to be added. It will not
 * be modified, as it is immutable.
 * @param object object to be added
 * @param key key for the object to be added
 */
+(NSDictionary*)dictionaryWithDictionary:(NSDictionary*)dictionary andSetObject:(id)object forKey:(id)key;

/** @abstract Constructor to a new immutable Dictionary by performing an Union operation
 * between two Dictionaries.
 *
 * @param dictionary Base Dictionary that needs an object to be added. It will not
 * be modified, as it is immutable.
 * @param unionDictionary Dictionary which contains the key-values to be added to dictionary
 */
+(NSDictionary*)dictionaryWithDictionary:(NSDictionary*)dictionary union:(NSDictionary*)unionDictionary;

@end
