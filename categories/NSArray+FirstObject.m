//
//  NSArray_FirstObject.h
//  http://troybrant.net/blog/2010/02/adding-firstobject-to-nsarray/

#import "NSArray+FirstObject.h"

@implementation NSArray (FirstObject)

- (id)firstObject
{
    if ([self count] > 0)
    {
        return [self objectAtIndex:0];
    }
    return nil;
}

@end 