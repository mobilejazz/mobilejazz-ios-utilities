//
//  NSDictionary+SetObject.m
//
//  Created by gimix on 8/23/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import "NSDictionary+SetObject.h"

@implementation NSDictionary (SetObject)

+(NSDictionary*)dictionaryWithDictionary:(NSDictionary*)dictionary andSetObject:(id)object forKey:(id)key
{
    NSMutableDictionary* tmp = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    [tmp setObject:object forKey:key];
    return tmp;
}

+(NSDictionary*)dictionaryWithDictionary:(NSDictionary*)dictionary union:(NSDictionary*)unionDictionary
{
    NSMutableDictionary* tmp = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    [tmp addEntriesFromDictionary:unionDictionary];
    return tmp;    
}

@end
