//
//  NSString+UInt64.h
//
//  Created by Stefan Klumpp on 2/28/12.
//  Copyright (c) 2012 Mobile Jazz, S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (uint64)

- (UInt64) uInt64Value;
+ (NSString *) stringFromUInt64:(UInt64)number;

@end
