//
//  UIView+FindUIViewController.h
//
//  Created by Jordi Giménez Gámez on 5/23/12.
//  Copyright (c) 2012 Mobile Jazz, S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

/** @see http://stackoverflow.com/questions/1340434/get-to-uiviewcontroller-from-uiview-on-iphone
*/
@interface UIView (FindUIViewController)

- (UIViewController *) firstAvailableUIViewController;

@end
