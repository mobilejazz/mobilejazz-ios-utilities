//
//  NSArray+Mapping.h
//
//  Created by Jordi Giménez Gámez on 3/23/12.
//  Copyright (c) 2012 Mobile Jazz, S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Mapping)

-(NSArray *)arrayByMappingObjectsWithBlock:(id (^)(id obj))block;

@end
