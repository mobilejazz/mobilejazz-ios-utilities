//
//  UIView+Frame.h
//
//  Created by gimix on 12/13/12.
//  Copyright (c) 2012 Mobile Jazz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

-(CGFloat)x;
-(CGFloat)y;
-(CGFloat)width;
-(CGFloat)height;
-(void)setX:(CGFloat)x;
-(void)setY:(CGFloat)y;
-(void)setWidth:(CGFloat)width;
-(void)setHeight:(CGFloat)height;

@end
