//
//  NSAttributedString+MJHTML.m
//
//  Created by gimix on 8/29/12.
//  Copyright (c) 2012 Mobile Jazz. All rights reserved.
//

#import "NSAttributedString+MJHTML.h"
#import "NSAttributedString+Attributes.h"

@implementation NSAttributedString (MJHTML)

+(NSAttributedString*) attributedStringWithHtml:(NSString*)html font:(UIFont*)font boldFont:(UIFont*)boldFont
{
    return [NSMutableAttributedString attributedStringWithHtml:html font:font boldFont:boldFont];
}

@end

@implementation NSMutableAttributedString (MJHTML)

+(NSMutableAttributedString*) attributedStringWithHtml:(NSString*)html font:(UIFont*)font boldFont:(UIFont*)boldFont
{
    NSMutableAttributedString* result = [[NSMutableAttributedString alloc] init];
    NSUInteger i = 0;
    BOOL bold = NO;
    while(i < html.length) {
        unichar c = [html characterAtIndex:i];
        if(c == '<' && i+3 <= html.length && [[html substringWithRange:NSMakeRange(i, 3)] isEqualToString:@"<b>"]) { // tag, process
            bold = YES;
            i+=3; // advance pointer
        } else if(c == '<' && i+4 <= html.length && [[html substringWithRange:NSMakeRange(i, 4)] isEqualToString:@"</b>"]) {
            bold = NO;
            i+=4; // advance pointer
        } else if(c == '<' && i+4 <= html.length && [[html substringWithRange:NSMakeRange(i, 4)] isEqualToString:@"<br>"]) {
            i+=4; // advance pointer
            c = '\n';
            NSMutableAttributedString *a = [NSMutableAttributedString attributedStringWithString:[NSString stringWithCharacters:&c length:1]];
            [a setFont:(bold ? boldFont : font)];
            [result appendAttributedString:a];
        } else if(c == '<' && i+5 <= html.length && [[html substringWithRange:NSMakeRange(i, 5)] isEqualToString:@"<br/>"]) {
            i+=5; // advance pointer
            c = '\n';
            NSMutableAttributedString *a = [NSMutableAttributedString attributedStringWithString:[NSString stringWithCharacters:&c length:1]];
            [a setFont:(bold ? boldFont : font)];
            [result appendAttributedString:a];
        } else {
            NSMutableAttributedString *a = [NSMutableAttributedString attributedStringWithString:[NSString stringWithCharacters:&c length:1]];
            [a setFont:(bold ? boldFont : font)];
            [result appendAttributedString:a];
            i++;
        }
    }
    return result;
}

@end
