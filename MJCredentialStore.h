//
//  MJCredentialStore.h
//
//  Created by gimix on 9/13/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Generic interface for saving and retrieving user data from
 * the keychain. Requires Security.framework.
 */
@interface MJCredentialStore : NSObject

/**
 *	@abstract Returns the singleton.
 */
+(MJCredentialStore*) sharedInstance;

/** Credentials to store (e.g. "username" => "joe", "password" => "secret"). Always use values that can be archived (e.g. do not use BOOL but a boolean NSValue).
 */
@property (strong, nonatomic) NSDictionary* credentials;

@end
