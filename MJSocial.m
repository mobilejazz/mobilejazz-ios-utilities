//
//  MJSocial.m
//
//  Created by gimix on 12/10/12.
//  Copyright (c) 2012 Mobile Jazz. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import "MJSocial.h"

@interface MJSocial() <MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) UIViewController* presentingVC;

@end

@implementation MJSocial

+(MJSocial*) sharedInstance
{
    // http://lukeredpath.co.uk/blog/a-note-on-objective-c-singletons.html
    static dispatch_once_t pred = 0;
    __strong static MJSocial* sharedObject = nil;
    dispatch_once(&pred, ^{
        sharedObject = [[MJSocial alloc] init];
		
    });
    return sharedObject;
}

-(BOOL)canSendEMail
{
    return [MFMailComposeViewController canSendMail];
}

- (void)sendEmail:(NSString*)body isHTML:(BOOL)isHTML withSubject:(NSString*)subject fromViewController:(UIViewController*)presentingVC
{
    self.presentingVC = presentingVC;
    
    // check availability
    if(! [self canSendEMail])
        return;
    // send
    MFMailComposeViewController *viewController = [[MFMailComposeViewController alloc] init];
    [viewController setSubject:subject];
    [viewController setMessageBody:body isHTML:isHTML];
    viewController.mailComposeDelegate = self;
    [self.presentingVC presentModalViewController:viewController animated:YES];
}

-(BOOL)canSendSMS
{
    return [MFMessageComposeViewController canSendText];
}

- (void)sendSMS:(NSString*)text fromViewController:(UIViewController*)presentingVC
{
    self.presentingVC = presentingVC;
    
    if(! [self canSendSMS])
        return;
    // send
    MFMessageComposeViewController *viewController = [[MFMessageComposeViewController alloc] init];
    viewController.body = text;
    viewController.messageComposeDelegate = self;
    [self.presentingVC presentModalViewController:viewController animated:YES];
}

-(void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self.presentingVC dismissModalViewControllerAnimated:YES];
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self.presentingVC dismissModalViewControllerAnimated:YES];
}

@end
