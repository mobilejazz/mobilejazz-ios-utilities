//
//  MJAttributedLabel.m
//
//  Created by gimix on 9/27/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import "MJAttributedLabel.h"

NSString* const kMJLinkAttributeName = @"MJLink";

@implementation MJAttributedLabel

-(void)setAttributedText:(NSAttributedString *)attributedText
{
    [super setAttributedText:attributedText];
    // insert links
    [attributedText enumerateAttribute:kMJLinkAttributeName inRange:NSMakeRange(0, attributedText.length)
                     options:kNilOptions usingBlock:^(NSURL* value, NSRange range, BOOL *stop)
    {
        if(value)
            [self addCustomLink:value inRange:range];
    }];
}

@end
