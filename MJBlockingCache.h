//
//  MJBlockingCache.h
//
//  Created by gimix on 9/13/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import <Foundation/Foundation.h>

/** A cache that can block readers of a key if there is a writer working to get 
 * the value of that key.
 * Also forgets entries on memory warning conditions.
 */
@interface MJBlockingCache : NSObject

/** Tries to get the value of a key from the cache. If the value is not cached,
 * it returns nil.
 * Never blocks, so it is safe to call from main thread.
 * @param key key to read
 * @return the cached value or nil
 */
-(id)getKeyFromCacheOnly:(NSString*)key;

/** Tries to get the value of a key from the cache. If the value is not cached,
 * calls the block to get it and caches the result for next times.
 * Blocks all concurrent calls for the same key in the meantime, so the block
 * gets executed the minimum possible amount of times. Blocks while
 * block is working. Do not call from main thread.
 * @param key key to read/write
 * @param block block that works and returns the value to write. Can
 * return nil and the key will not be set.
 * @return the cached value or result of block execution
 */
-(id)getKeyFromCache:(NSString*)key OrFromBlock:(id (^)())block;

/** Tries to get the value of a key from the cache without blocking. 
 * If the value is cached, thenBlock is executed immediately on the current thread.
 * If the value is not cached, calls calculationBlock in a background thread to get it and caches the result for next times.
 * Blocks all lookups for the same key in the meantime, so calculationBlock gets executed the minimum possible amount of times.
 * Never blocks, so it is safe to call from main thread.
 * It is a convenience method for a common pattern: get stuff from the cache, 
 * calculate it in the background thread if not found, then use the result in 
 * the main thread. Presents the advantage of not switching threads if not necessary.
 * @param key key to read/write
 * @param block block that works and returns the value to write. Can
 * return nil and the key will not be set.
 * @return the cached value or result of block execution
 */
-(void)getKeyFromCache:(NSString*)key OrFromBlock:(id (^)())calculationBlock ThenExecute:(void (^)(id value))thenBlock;

/** Forgets all entries */
-(void)clean;

@end
