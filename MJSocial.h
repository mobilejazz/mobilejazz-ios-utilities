//
//  MJSocial.h
//
//  Created by gimix on 12/10/12.
//  Copyright (c) 2012 Mobile Jazz. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Sharing functionality */
@interface MJSocial : NSObject

/**
 *	@abstract Returns the singleton
 */
+(MJSocial*) sharedInstance;

/** Sends an e-mail
 * @param presentingVC ViewController that will be used to present the e-mail
 * screen modally. Usually you pass the current viewcontroller here.
 */
- (void)sendEmail:(NSString*)body isHTML:(BOOL)isHTML withSubject:(NSString*)subject fromViewController:(UIViewController*)presentingVC;

-(BOOL)canSendEMail;

/** Sends an SMS message
 * @param presentingVC ViewController that will be used to present the SMS
 * screen modally. Usually you pass the current viewcontroller here.
 */
- (void)sendSMS:(NSString*)text fromViewController:(UIViewController*)presentingVC;

-(BOOL)canSendSMS;

@end
