//
//  MJAttributedLabel.h
//
//  Created by gimix on 9/27/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import "OHAttributedLabel.h"

extern NSString* const kMJLinkAttributeName;

/** OHAttributedLabel that supports links in the NSAttributedString.
 * Use the `kMJLinkAttributeName` attribute to specify the ranges that correspond
 * to a link and an NSURL as value.
 */
@interface MJAttributedLabel : OHAttributedLabel

@end
