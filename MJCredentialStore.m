//
//  MJCredentialStore.m
//  dtime
//
//  Created by gimix on 9/13/12.
//  Copyright (c) 2012 dtime. All rights reserved.
//

#import "MJCredentialStore.h"

// Used to specify the application used in accessing the Keychain.
#define APP_NAME [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]
#define ACCOUNT @"credentials"

// disable when in production, as this would tell secrets to the system log
#define CREDENTIALS_DEBUG NO

@implementation MJCredentialStore

+(MJCredentialStore*) sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static MJCredentialStore* sharedObject = nil;
    dispatch_once(&pred, ^{
        sharedObject = [[MJCredentialStore alloc] init];
    });
    return sharedObject;
}


- (void)setCredentials:(NSDictionary*)credentials
{
    if(!credentials) {
        // delete instead of saving
        [self clean];
        return;
    }
    
	// Dictionary with all the account data will be stored as password
    // account has a fixed name (ACCOUNT), so it always retrieves this one
    
	// Build the keychain query
    NSMutableDictionary *keychainQuery = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          (__bridge_transfer NSString *)kSecClassGenericPassword, (__bridge_transfer NSString *)kSecClass,
                                          APP_NAME, kSecAttrService,
                                          ACCOUNT, kSecAttrAccount,
                                          kCFBooleanTrue, kSecReturnAttributes,
                                          nil];
    
    // Delete an existing entry first:
    SecItemDelete((__bridge_retained CFDictionaryRef) keychainQuery);
    
    // Add the accounts query to the keychain query:
    [keychainQuery setObject:[NSKeyedArchiver archivedDataWithRootObject:credentials]
                      forKey:(__bridge_transfer NSString *)kSecValueData];
    
    // Add the token data to the keychain
    // Even if we never use resData, replacing with NULL in the call throws EXC_BAD_ACCESS
    CFTypeRef resData = NULL;
    SecItemAdd((__bridge_retained CFDictionaryRef)keychainQuery, (CFTypeRef *) &resData);
	
    if(CREDENTIALS_DEBUG)
        MJLog(@"Storing keychain entry: %@", keychainQuery);
}

-(NSDictionary*)credentials
{
	NSMutableDictionary *keychainQuery = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          (__bridge_transfer NSString *)kSecClassGenericPassword, (__bridge_transfer NSString *)kSecClass,
                                          APP_NAME, kSecAttrService,
                                          ACCOUNT, kSecAttrAccount,
                                          kCFBooleanTrue, kSecReturnData,
										  kSecMatchLimitOne, kSecMatchLimit,
                                          nil];
    
    // Get the token data from the keychain
    CFTypeRef resData = NULL;
    
    // Get the token dictionary from the keychain
    if (SecItemCopyMatching((__bridge_retained CFDictionaryRef) keychainQuery, (CFTypeRef *) &resData) == noErr)
    {
        NSData *resultData = (__bridge_transfer NSData *)resData;
        
        if(CREDENTIALS_DEBUG)
            MJLog(@"Got keychain entry: %@", resultData);
        if (resultData) {
            NSDictionary *account = [NSKeyedUnarchiver unarchiveObjectWithData:resultData];
            return account;
        }
    }
    return nil;
}

-(void)clean
{
    // Build the keychain query
    NSMutableDictionary *keychainQuery = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          (__bridge_transfer NSString *)kSecClassGenericPassword, (__bridge_transfer NSString *)kSecClass,
                                          APP_NAME, kSecAttrService,
                                          ACCOUNT, kSecAttrAccount,
                                          kCFBooleanTrue, kSecReturnAttributes,
                                          nil];
    
    // Delete entry:
    SecItemDelete((__bridge_retained CFDictionaryRef) keychainQuery);
}

@end
