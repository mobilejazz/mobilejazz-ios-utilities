//
//  Created by Jordi Giménez Gámez on 5/16/12.
//  Copyright (c) 2012 Mobile Jazz, S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActivityIndicatorButtonItem : UIBarButtonItem

@end
