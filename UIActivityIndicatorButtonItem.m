//
//  Created by Jordi Giménez Gámez on 5/16/12.
//  Copyright (c) 2012 Mobile Jazz, S.L. All rights reserved.
//

#import "UIActivityIndicatorButtonItem.h"

@implementation UIActivityIndicatorButtonItem

-(void) awakeFromNib
{
    UIActivityIndicatorView* loadingView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [loadingView sizeToFit];  
    loadingView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);  
    
    [loadingView startAnimating];
    self.customView = loadingView;
}

@end
